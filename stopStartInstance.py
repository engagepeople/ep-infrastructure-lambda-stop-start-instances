import boto3
import logging
import time
from time import sleep
##
# This is a simple Lambda function to filter the EC2 instances that contain a tag named `scheduled` which is set to `true`.
# If that condition is meet function will compare current time (H:M) to a value of the additional tags which defines the trigger `scheduleStop` or `scheduleStart`.
# Value of the `ScheduleStop` or `ScheduleStart` must be in the following format `H:M` - example `09:00`  
# 
# The Lambda function will be executed every 15 minutes. 
# Following Lambda Function needs a role with permission to start and stop EC2 instances and writhe to CloudWatch logs.
# 
# Example EC2 Instance tags: 
# 
# Scheduled     : True
# ScheduleStart : 06:00 
# ScheduleStop  : 18:15
##
#define boto3 the connection to collect region information
ec2r = boto3.client('ec2')
# Define array
stopInstances = []   
startInstances = []  
def lambda_handler(event, context):
    # Get current time in format H:M
    current_time = time.strftime("%H:%M")
    print("Execution time: ",current_time)
    # Describe all regions
    regions = ec2r.describe_regions()
    for region in regions['Regions']:
        print ("Working on region:",region['RegionName'])
        # Search all the instances which contains scheduled filter 
        ec2c = boto3.client('ec2',region_name=region['RegionName'])
        instances = ec2c.describe_instances(
            Filters = [{
                'Name': 'tag:scheduled',
                'Values': ['true']
                }
            ]
        )
        if len(instances['Reservations']) == 0:
            print ("No instances with scheduled tag found on the region: ", region['RegionName'])
        else:
            for instance in instances['Reservations']:
                for inst in instance['Instances']:
                    for instTag in inst['Tags']:
                        if instTag['Key'] == 'scheduleStop' and instTag['Value'] == current_time:
                            print ("Identified instance : ", inst['InstanceId'], "to stop at: ", current_time)
                            stopInstances.append(inst['InstanceId'])
                        if instTag['Key'] == 'scheduleStart' and instTag['Value'] == current_time:
                            print ("Identified instance : ", inst['InstanceId'], "to start at: ", current_time)
                            startInstances.append(inst['InstanceId'])
                        if len(stopInstances) == 0:
                            pass
                        else:
                            stop = ec2c.stop_instances(
                                InstanceIds = [
                                    *stopInstances
                                ]
                            )
                            for stopEc2 in stopInstances:
                                ec2StopInst = ec2c.describe_instances(
                                    InstanceIds=[
                                        stopEc2,
                                    ]
                                )
                                for instStop in ec2StopInst['Reservations']:
                                    for stoppedInst in instStop['Instances']:
                                        count=10
                                        while (stoppedInst['State']['Name'] != 'stopped' and count > 0):
                                            print (stoppedInst['State']['Name'])
                                            sleep(5)
                                            count=count-1
                        if len(startInstances) == 0:
                            pass
                        else:
                            start = ec2c.start_instances(
                                InstanceIds = [
                                    *startInstances
                                ]
                            )
                            for startEc2 in startInstances:
                                ec2StartInst = ec2c.describe_instances(
                                    InstanceIds=[
                                        startEc2,
                                    ]
                                )
                                for instStart in ec2StartInst['Reservations']:
                                    for startedInst in instStart['Instances']:
                                        count=10
                                        while (startedInst['State']['Name'] != 'running' and count > 0):
                                            print (startedInst['State']['Name'])
                                            sleep(5)
                                            count=count-1
